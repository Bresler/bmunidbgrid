unit Main;

interface

uses

  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, Data.DB, Datasnap.DBClient,
  uniGUIBaseClasses, uniBasicGrid, uniDBGrid,
  uniButton, uniBitBtn, uniMemo, uniPanel, uniDBNavigator,
  uniMenuButton, BMUniDbGrid, System.Actions, Vcl.ActnList, Vcl.Menus,
  uniMainMenu;

type
  TMainForm = class(TUniForm)
    clientdata: TClientDataSet;
    dclientdata: TDataSource;
    memo: TUniMemo;
    UniDBNavigator1: TUniDBNavigator;
    SelectAllbtn: TUniMenuButton;
    unselectallbtn: TUniBitBtn;
    UniBitBtn1: TUniBitBtn;
    grid: TBMUniDBGrid;
    clientdataNAME: TStringField;
    clientdataSIZE: TSmallintField;
    clientdataWEIGHT: TSmallintField;
    clientdataAREA: TStringField;
    clientdataBMP: TBlobField;
    UniPopupMenu1: TUniPopupMenu;
    ActionList1: TActionList;
    Action1: TAction;
    Action2: TAction;
    Action11: TUniMenuItem;
    Action21: TUniMenuItem;
    procedure UniFormCreate(Sender: TObject);
    procedure gridButtonAction(Sender: TObject; ButtonEvent: string);
    procedure SelectAllbtnClick(Sender: TObject);
    procedure unselectallbtnClick(Sender: TObject);
    procedure UniBitBtn1Click(Sender: TObject);
    procedure Action1Execute(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

function MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  uniGUIVars, MainModule, uniGUIApplication;

function MainForm: TMainForm;
begin
  Result := TMainForm(UniMainModule.GetFormInstance(TMainForm));
end;


procedure TMainForm.Action1Execute(Sender: TObject);
begin
  ShowMessage('Action1');
//  grid.HideMsg(ButtonEvent);
end;

procedure TMainForm.gridButtonAction(Sender: TObject; ButtonEvent: string);
begin
   memo.Lines.Add('Clik :  ' +  ButtonEvent   + ' ===> ' +  clientdata.fieldbyname('name').asstring +'   Area :  ' +
                                  clientdata.fieldbyname('area').asstring)  ;
    //Simulating a operation
   sleep(2000);
   grid.HideMsg('AcceptButton');
end;

procedure TMainForm.SelectAllbtnClick(Sender: TObject);
begin
 grid.selectAll() ;


end;

procedure TMainForm.UniBitBtn1Click(Sender: TObject);
 var i, LCount : integer ;
   CurrentPos: TBookmark;
begin
  Memo.Clear ;
//  memo.Lines.Add('SelectedRows : ' +  inttostr( grid.GetSelectionCount)) ;
  LCount := grid.BookMarkList.Count;
  memo.Lines.Add('SelectedRows : ' +  inttostr( LCount)) ;
//  if grid.GetSelectionCount > 0 then begin
  if LCount > 0 then begin
    grid.DataSource.DataSet.DisableControls;
    CurrentPos := grid.DataSource.DataSet.Bookmark;
//    for i := 0 to  grid.GetSelectionCount - 1 do begin
//      grid.DataSource.DataSet.Bookmark := grid.GetBookmark(i);
    for i := 0 to LCount - 1 do begin
      grid.DataSource.DataSet.Bookmark := grid.BookMarkList.GetBookmark(I);


     memo.lines.add('Name : ' +   clientdata.fieldbyname('name').asstring +'   Area :  ' +
                                  clientdata.fieldbyname('area').asstring)  ;
    end;
    grid.DataSource.DataSet.Bookmark := CurrentPos;
    grid.DataSource.DataSet.EnableControls;
    grid.unselectAll ;
//    FreeAndNil(CurrentPos);
  end;


end;

procedure TMainForm.UniFormCreate(Sender: TObject);
begin

   Clientdata.Open ;
   memo.Clear ;
end;

procedure TMainForm.unselectallbtnClick(Sender: TObject);
begin
  grid.unselectAll ;
end;

initialization
  RegisterAppFormClass(TMainForm);

end.
