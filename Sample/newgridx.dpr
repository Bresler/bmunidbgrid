program newgridx;


uses
//  DEBUGGING with FastMM4
//  FastMM4,
//  FastMM4Messages,
  Forms,
  ServerModule in 'ServerModule.pas' {UniServerModule: TUniGUIServerModule},
  MainModule in 'MainModule.pas' {UniMainModule: TUniGUIMainModule},
  Main in 'Main.pas' {MainForm: TUniForm};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := true;
  Application.Initialize;
  TUniServerModule.Create(Application);
  Application.Run;
end.
