// ------------------------------------------------------------------------------
// BMUniDBGrid Component
//
// Developed By:  Maikel del Valle Bressler and Salvatore Marullo
//
// Email: rullomare@gmail.com
//
// Email: mvbressler2008@gmail.com
//
// Last Update: 2015-06
// Unigui Version  099..... Delphi XE to XE8
// ------------------------------------------------------------------------------
//
// Free to use
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//
// Add PopUpMenu  (clone  code from Mr. PatMap) 2015-02-06     // deleted Version 090.1176
// ------------------------------------------------------------------------------
//
Unit BMUniDBGrid;
{$I uniCompilers.inc}

Interface

Uses
  SysUtils, Classes, UniGUIInterfaces, uniGUITypes, DB, uniGUIClasses,
  UniGUIAbstractClasses, uniBitBtn, uniDBUtils, UniBasicGrid, UniComboBox,
  UniMainMenu, UniDBGrid;

type
  Tx = class(TUniPopupMenu);
  TColPosition = (First, Last);
  TSearchPosition = (Pbottom, Ptop);

{$REGION 'ActionItem'}

  ActionItem = class(TCollectionItem)
  private
    FToolTip: String;
    FIconFilename: String;
    FButtonName: String;
  public
    constructor Create(ACollection: TCollection); override;
    destructor Destroy; override;
  published
    property ToolTip: String read FToolTip write FToolTip;
    property ButtonName: String read FButtonName write FButtonName;
    property IconFilename: string read FIconFilename write FIconFilename;
  end;
{$ENDREGION}

{$REGION 'TActionItems'}

  TActionItems = class(TCollection)
  private
    function GetItem(Index: Integer): ActionItem;
    procedure SetItem(Index: Integer; const Value: ActionItem);
  public
    function Add: ActionItem;
    function Insert(Index: Integer): ActionItem;
    property Items[Index: Integer]: ActionItem read GetItem write SetItem;
  end;
{$ENDREGION}

{$REGION 'TSearchOptions'}

  TSearchOptions = class(TPersistent)
  private
    FSearchColumns: string;
    FReconfigureColsMenu: boolean;
    FSearchButtonTip: string;
    FClearButtonTip: string;
    FReconfigureMenuButtonTip: string;
    FSearchFieldLabel: String;
    FSearchTipText: String;
    FSearchFieldWidth: Integer;
    FsearchMinLength: Integer;
    FautoSearch: boolean;
    FAutoSearchTipText: string;
    FsearchPosition: TSearchPosition;

  Public
    constructor Create;
    destructor Destroy;
  Published
    { Published declarations }
    Property SearchColumns: string read FSearchColumns write FSearchColumns;
    Property ReconfigureColsMenu: boolean read FReconfigureColsMenu
      write FReconfigureColsMenu;
    Property SearchButtonTip: string read FSearchButtonTip
      write FSearchButtonTip;
    Property ClearButtonTip: string read FClearButtonTip write FClearButtonTip;
    Property ReconfigureButtonTip: string read FReconfigureMenuButtonTip
      write FReconfigureMenuButtonTip;
    Property SearchFieldLabel: string read FSearchFieldLabel
      write FSearchFieldLabel;
    Property SearchTipText: string read FSearchTipText write FSearchTipText;
    Property SearchFieldwidth: Integer read FSearchFieldWidth
      write FSearchFieldWidth;
    Property SearchMinLength: Integer read FsearchMinLength
      write FsearchMinLength;
    Property AutoSearch: boolean read FautoSearch write FautoSearch;
    Property AutoSearchTipText: string read FAutoSearchTipText
      write FAutoSearchTipText;
    Property SearchPosition: TSearchPosition Read FsearchPosition
      write FsearchPosition;
  end;
{$ENDREGION}

{$REGION 'TMyBookmarkList'}
  // type TMyBookmarkList = class
  // Private
  // FBookmarkList: TList<TBookMark>;
  /// /      FRawValueOfIndex: String; // Will by like this '0,1,2,3' indicating the indexes
  // FRawArrayOfIndex: TStringList;
  /// /      procedure SetRawArrayOfIndex(const AValue: TStringList);
  // Public
  // constructor Create;
  // destructor Destroy;
  // function GetBookmark(const Index: Integer):TBookmark;
  // procedure AddBookmark(const BM: TBookmark);
  // procedure ClearBookmarkList;
  /// /      function Find(BM: TBookmark): Boolean;
  // function Count: Integer;
  /// /      property RawValueOfIndex: String read FRawValueOfIndex write FRawValueOfIndex;
  // property RawArrayOfIndex: TStringList read FRawArrayOfIndex; //write SetRawArrayOfIndex;
  // property BookmarkList: TList<TBookMark> read FBookmarkList;
  // end;
{$ENDREGION}

Type
  TMyButtonActionEvent = procedure(Sender: TObject; ButtonEvent: string)
    of object;

  TBMUniDBGrid = Class(TUniDBGrid)
  Private
    { Private declarations }
    FSearchOptions: TSearchOptions;
    FActionColumnWidth: Integer;
    FActionColumnEvent: String;
    FActionButtons: TActionItems;
    FOnButtonAction: TMyButtonActionEvent;
    FActionColumnPos: TColPosition;

{$REGION 'Removed'}
    // FCheckBoxSelect          : boolean ;
    // FMultiSelectRows         : boolean ;
    // FSelectionListObject     : TMyBookmarkList;
{$ENDREGION}

    FPagingToolbarResizer: boolean;
    FProgressBarPager: boolean;
    FPopupMenu: TUniPopupMenu;
    FSearchfeature: boolean;

{$REGION 'Removed'}
    // function RowMultiSelect:Boolean; override;
{$ENDREGION}
  Protected
    { Protected declarations }
{$REGION 'Removed'}
    // procedure ClearSelectionList;
{$ENDREGION}
    procedure JSEventHandler(AEventName: String; AParams: TUniStrings);
      override;
    procedure ConfigJSClasses(ALoading: boolean); override;

    // This procedure is called only in web mode
    // It will be called when all design time properties are loaded
    // and Ext JS compoenent is fully initialized
    procedure LoadCompleted; override;
    Procedure Loaded; override;

{$REGION 'Removed'}
    // WebCreate is called only in web mode. It is called once after component is created
    // procedure WebCreate; override;
    // function GetBookmarkList: TArray<TBookMark>;
{$ENDREGION}
    Procedure AfterRender(This: TJSObject; EventName: string;
      Params: TUniStrings);
  Public
    { Public declarations }
{$REGION 'Removed'}
    // SelectedRows : integer ;
{$ENDREGION}
    Constructor Create(Owner: TComponent); Override;
    destructor Destroy; override;
{$REGION 'Removed'}
    // function    GetBookmark(Index: Integer):TBookmark;
    // function    GetRowFielValuedByRowIndex(SelectedRowIndex, FieldIndex: Integer): Variant; overload;
    // function    GetRowFielValuedByRowIndex(SelectedRowIndex: Integer; FieldName: String): Variant; overload;
    // procedure   AddBookmark(BM: TBookmark);
    // function    GetSelectionCount: Integer;
    // procedure   Select(BM: TBookmark; keepExisting, FireEvent: Boolean ); overload;
    // procedure   Select(RawValue: String; const keepExisting: Boolean = False; const FireEvent: Boolean = False); overload;
    // procedure   Select(BMList: TArray<TBookMark>); overload;
{$ENDREGION}
    procedure unselectAll;
    procedure selectAll;
{$REGION 'Removed'}
    // property    BookmarkList: TArray<TBookMark> read GetBookmarkList;
    // property BookMarkList: TMyBookmarkList read FSelectionListObject;
{$ENDREGION}
  Published
    { Published declarations }
    property ActionButtons: TActionItems read FActionButtons
      write FActionButtons;
    property ActionColumnWidth: Integer read FActionColumnWidth
      write FActionColumnWidth;
    property ActionColumnEvent: String read FActionColumnEvent
      write FActionColumnEvent;
    property OnButtonAction: TMyButtonActionEvent read FOnButtonAction
      write FOnButtonAction;

    property ActionColumnPos: TColPosition read FActionColumnPos
      write FActionColumnPos;
    Property PagingToolbarResizer: boolean read FPagingToolbarResizer
      write FPagingToolbarResizer;
    Property ProgressbarPager: boolean read FProgressBarPager
      write FProgressBarPager;
    Property PopupMenu: TUniPopupMenu read FPopupMenu Write FPopupMenu;
{$REGION 'Removed'}
    // property CheckBoxSelect          : boolean                read FCheckBoxSelect          write FCheckBoxSelect ;
    // Property MultiSelectRows         : boolean                read FMultiSelectRows         Write FMultiSelectRows;
{$ENDREGION}
    property Searchfeature: boolean read FSearchfeature write FSearchfeature;
    property SearchOptions: TSearchOptions read FSearchOptions
      write FSearchOptions;
  End;

Implementation

Constructor TSearchOptions.Create;
Begin
  // inherited ;
  FSearchColumns := '';
  FReconfigureColsMenu := false;
  FSearchButtonTip := '';
  FClearButtonTip := '';
  FReconfigureMenuButtonTip := '';
  FSearchFieldLabel := '';
  FSearchTipText := '';
  FsearchMinLength := 2;
  FSearchFieldWidth := 250;
  FautoSearch := false;
  FAutoSearchTipText := 'Type at least {0} characters';
End;

destructor TSearchOptions.Destroy;
begin
  inherited;
end;

{$REGION 'TActionItems'}

function TActionItems.Add: ActionItem;
begin
  Result := ActionItem(inherited Add);
end;

function TActionItems.GetItem(Index: Integer): ActionItem;
begin
  Result := ActionItem(inherited Items[Index]);
end;

function TActionItems.Insert(Index: Integer): ActionItem;
begin
  Result := ActionItem(inherited Insert(Index));
end;

procedure TActionItems.SetItem(Index: Integer; const Value: ActionItem);
begin
  Items[Index].Assign(Value);
end;
{$ENDREGION}
{$REGION 'ActionItem'}

constructor ActionItem.Create(ACollection: TCollection);
begin
  inherited Create(ACollection);
end;

destructor ActionItem.Destroy;
begin
  inherited;
end;
{$ENDREGION}
{ TBMUniDBGrid }

procedure TBMUniDBGrid.ConfigJSClasses(ALoading: boolean);
begin
  JSObjects.DefaultJSClassName := 'Ext.grid.Panel';
  JSObjects.JSClasses['cellModel'] := 'Ext.selection.CellModel';
  JSObjects.JSClasses['rowModel'] := 'Ext.selection.RowModel';
  JSObjects.JSClasses['checkboxModel'] := 'Ext.selection.CheckboxModel';
  JSObjects.JSClasses['store'] := 'Ext.data.Store';
  JSObjects.JSClasses['pagingBar'] := 'Ext.toolbar.Paging';
end;

{$REGION 'Removed'}
(*
  // WebCreate is called only in web mode. It is called once after component is created
  procedure TBMUniDBGrid.WebCreate;
  begin
  inherited;
  end;
*)
{$ENDREGION}

Procedure TBMUniDBGrid.Loaded;
var
  JSFunctionBody, JSPlugins: String;
begin

  inherited;

  if (self.WebOptions.Paged and FPagingToolbarResizer) then
  begin
    if FProgressBarPager then
    begin
      // This is to concatenate code at the end
      JSFunctionBody :=
        'if (sender.pagingBar){ sender.pagingBar.removeAll(true); }' +
        'sender.addDocked({' + ' dock: "bottom",' +
        ' items: [ Ext.create("Ext.PagingToolbar", { ' +
        '   pageSize: sender.store.pageSize, ' + '   store: sender.store,' +
        '   displayInfo: true, ' + '   plugins: [ ' +
        '     Ext.create("Ext.ux.PagingToolbarResizer", {' +
        '       displayText: ''Records per Page'', ' +
        '       options : [ 5, 10, 15, 20, 25,100,200,300,500,1000,2000,5000 ]}),'
        + '     Ext.create(''Ext.ux.ProgressBarPager'', {})]' + // end plugins
        '   })] ' + // End Items
        '});'; // addDocked

    end
    else
    begin
      // This is to concatenate code at the end
      JSFunctionBody :=
        'if (sender.pagingBar) { sender.pagingBar.removeAll(true); }' +
        'sender.addDocked({' + '  dock: "bottom", ' +
        '  items: [Ext.create("Ext.PagingToolbar", { ' +
        '   pageSize: sender.store.pageSize, ' + '   store: sender.store,' +
        '   displayInfo: true, ' +
        '   plugins: [Ext.create("Ext.ux.PagingToolbarResizer", {' +
        '    displayText: "Records per Page", ' +
        '    options : [ 5, 10, 15, 20, 25,100,200,300,500,1000,2000,5000 ]})' +
        ' ]' + // end plugins
        ' })] });';

    end;
  end
  else if FProgressBarPager then
  begin
    // This is to concatenate code at the end
    JSFunctionBody :=
      'if (sender.pagingBar) { sender.pagingBar.removeAll(true); } ' +
      'sender.addDocked({' + ' dock: "bottom",' +
      ' items: [Ext.create("Ext.PagingToolbar", { ' +
      '   pageSize: sender.store.pageSize, ' + '   store: sender.store,' +
      '   displayInfo: true, ' +
      '   plugins: [ Ext.create(''Ext.ux.ProgressBarPager'', {})]' +
    // end plugins
      ' })] });';
  end;

{$REGION 'Removed'}
  // This is for showing the CheckBox header for Selecting and UnSelecting All records
  // if (CheckBoxSelect and  RowMultiSelect )   then
  // JSFunctionBody := JSFunctionBody +
  // 'sender.getSelectionModel().showHeaderCheckbox = true;';

{$ENDREGION}
  // Adding the BeforeRender with the function created
  if JSFunctionBody <> '' then
    JSAddListener('beforerender', JSFunction('sender, eOpts', JSFunctionBody));

  If Assigned(FPopupMenu) And (FPopupMenu.Items.Count > 0) Then
  Begin
    JSAddEvent('afterrender', ['This', '%0.nm'], AfterRender, JSGridPanel);
  End;
  JSFunctionBody := EmptyStr;
  JSPlugins := EmptyStr;
end;

{$REGION 'Removed'}
// function TBMUniDBGrid.RowMultiSelect: Boolean;
// begin
// Result := dgMultiSelect in Options;
// end;

// procedure TBMUniDBGrid.Select(BM: TBookmark; keepExisting, FireEvent: Boolean);
// begin
//
// end;

// procedure TBMUniDBGrid.Select(BMList: TArray<TBookMark>);
// begin
// FSelectionListObject.Destroy;
// end;
{$ENDREGION}

Procedure TBMUniDBGrid.AfterRender;
Var
  JSName, JSNamePopUp: String;
  I: Integer;
Begin

  If Assigned(FPopupMenu) Then
  Begin
    JSNamePopUp := Tx(FPopupMenu).GetMenuControl.JSName;
    JSName := StringReplace(JSGridPanel.JSName, #4, '', [RfReplaceAll]);
    JSCall('getEl().on', ['contextmenu', JSFunction('e, evtObj',
      'var SelNode =  ' + JSName +
      '.getSelectionModel().getSelection()[0]; if(SelNode != undefined){ ' +
      JSName + '.fireEvent("itemclick", ' + JSName +
      ', SelNode);} e.stopEvent(); ' + JSNamePopUp +
      '.showAt([e.getPageX(), e.getPageY()]);')]);
  End;
  JSName := EmptyStr;
  JSNamePopUp := EmptyStr;
End;

// This procedure is called only in web mode
// It will be called when all design time properties are loaded
// and Ext JS compoenent is fully initialized
procedure TBMUniDBGrid.LoadCompleted;
var
  I: Integer;
  Cols: String;

  checkIndexes: String;
  ii: Integer;
  ts: TStrings;
  TmpStr: String;
  AutoSearchNumber: Integer;
  Position: String;
begin
  AutoSearchNumber := 0;
  if FActionColumnEvent = EmptyStr then
    FActionColumnEvent := 'ColsActionEvent';

{$REGION 'Removed'}
  // if FCheckBoxSelect  then  begin
  // Self.Options := Self.Options + [TUniDBGridOption.dgRowSelect];
  // Self.Options := Self.Options + [TUniDBGridOption.dgMultiSelect];
  // end;
{$ENDREGION}
  if FActionButtons.Count > 0 then
  begin
    Cols := ' eColumn = new Ext.grid.column.Action( {' +
      'xtype: ''actioncolumn'',' + 'width: ' + IntToStr(FActionColumnWidth) +
      ', sortable : false , menuDisabled : true ,' + 'items:[';
    for I := 0 to FActionButtons.Count - 1 do
    begin
      Cols := Cols + ' {  icon: "' +
        StringReplace(FActionButtons.Items[I].FIconFilename, '\', '/',
        [RfReplaceAll]) + '",' + 'tooltip: "' + FActionButtons.Items[I].ToolTip
        + '",' + 'handler: function (grid , rowIndex, colIndex) {' +
        'grid.getSelectionModel().select(rowIndex);' + 'ajaxRequest(' +
        self.JSName + ',"' + FActionColumnEvent + '",["Buttons=" + "' +
        FActionButtons.Items[I].ButtonName + '"]);' + '}},';
    end;

    if FActionColumnPos = Last then

      Cols := copy(Cols, 1, length(Cols) - 1) + ' ]} ); columns.push(eColumn) ;'
    else
      Cols := copy(Cols, 1, length(Cols) - 1) +
        ' ]} ); columns.unshift(eColumn) ;';
    JSAddListener('beforereconfigure',
      JSFunction('sender, store, columns, oldStore, the, eOpts', Cols));
  end;

{$REGION 'Removed'}
  // JSAddListener('selectionchange', JSFunction('sender, selected, eOpts',
  //
  // 'var ArrayOfIndex = []; for (var i in selected){ArrayOfIndex.push(selected[i].index);}'
  // + ' ajaxRequest(sender, "selectionchange", ["ArrayOfIndex="+Ext.encode(ArrayOfIndex)]);')
  // );
{$ENDREGION}
  if FSearchfeature then
  begin
    checkIndexes := EmptyStr;

    with FSearchOptions do
    begin
      if FSearchColumns <> EmptyStr then
      begin
        ts := TStringList.Create;
        try
          ts.Delimiter := ',';
          ts.DelimitedText := FSearchColumns;
          for ii := 0 to ts.Count - 1 do
          begin
            TmpStr := TmpStr + '"' + ts[ii] + '",';
          end;
          TmpStr := copy(TmpStr, 1, length(TmpStr) - 1);
        finally
          ts.Free;
        end;

      end;

      if FClearButtonTip = EmptyStr then
        FClearButtonTip := Format('"%s"', ['Clear Search'])
      else
        FClearButtonTip := Format('"%s"', [FClearButtonTip]);
      if FSearchButtonTip = EmptyStr then
        FSearchButtonTip := Format('"%s"', ['Search'])
      else
        FSearchButtonTip := Format('"%s"', [FSearchButtonTip]);

      if FReconfigureMenuButtonTip = EmptyStr then
        FReconfigureMenuButtonTip := Format('"%s"', ['Reconfigure Search Menu'])
      else
        FReconfigureMenuButtonTip :=
          Format('"%s"', [FReconfigureMenuButtonTip]);
      if FSearchFieldLabel = EmptyStr then
        FSearchFieldLabel := Format('"%s"', ['Search'])
      else
        FSearchFieldLabel := Format('"%s"', [FSearchFieldLabel]);
      if FSearchTipText = EmptyStr then
        FSearchTipText := Format('"%s"',
          ['Type a text to search and press Enter'])
      else
        FSearchTipText := '"' + FSearchTipText + '"';

      if ((self.Columns.Count > 0) and (FSearchColumns = EmptyStr)) then
      begin

        for ii := 0 to self.Columns.Count - 1 do
        begin
          checkIndexes := checkIndexes + '"' + self.Columns.Items[ii]
            .fieldname + '",';
        end;
        checkIndexes := copy(checkIndexes, 1, length(checkIndexes) - 1);
        checkIndexes := '[' + checkIndexes + ']';
      end
      else if FSearchColumns <> EmptyStr then
      begin
        checkIndexes := TmpStr;
        checkIndexes := '[' + checkIndexes + ']';
      end
      else
        checkIndexes := '"all"';
      if FautoSearch then
        AutoSearchNumber := FsearchMinLength;
      if FsearchPosition = Ptop then
        Position := '"top"'
      else
        Position := '"bottom"';
      jsconfig('features',
        '[Ext.create(''Ext.ux.grid.feature.Searching'',{ftype: ''searching'',' +
        ' reconfigureColsMenu : ' +
        lowercase(booltostr(FReconfigureColsMenu, true)) +

        ',searchText: ' + FSearchFieldLabel + ', trigger1ButtonTip : ' +
        FClearButtonTip + ',trigger2ButtonTip : ' + FSearchButtonTip +
        ',trigger3ButtonTip : ' + FReconfigureMenuButtonTip +
        ',searchTipText:  ' + FSearchTipText + ',minLength : ' +
        IntToStr(FsearchMinLength) + ',width : ' + IntToStr(FSearchFieldWidth) +
        ',minChars:' + IntToStr(AutoSearchNumber) + ',minCharsTipText : "' +
        FAutoSearchTipText + '",position : ' + Position + ',checkIndexes : ' +
        checkIndexes + ',mode: "local"})]');

      jsconfig('stateful', [true]);

      jsconfig('stateId', '"stateGrid"');

      // ?????  to check
      JSAddListener('reconfigure',
        JSFunction('sender, store, columns, oldStore, the, eOpts',
        'Ext.ns(''My.Columns'') ;' +

        '  My.Columns.Fields = [] ; ' +

        ' for (i = 0; i < columns.length; i++) { ' +
        ' My.Columns.Fields.push(columns[i].text) ;}'));

      // ts.Free ;

    end;
  end;

  // Search Feauture
  inherited;

{$REGION 'Removed'}
  // if FmultiSelectRows  then
  // JSCall('getSelectionModel().setSelectionMode', ['MULTI']);
{$ENDREGION}
  Cols := EmptyStr;
  TmpStr := EmptyStr;
end;

{$REGION 'Removed'}
(*
  procedure TBMUniDBGrid.Select(BM: TBookmark; keepExisting, FireEvent: Boolean );
  var
  I: Integer;
  RawValue: String;
  CurrentPos: TBookmark;
  LocalDisabled: Boolean;
  begin
  LocalDisabled :=  false;
  CurrentPos := Self.DataSource.DataSet.Bookmark;
  if not DataSource.DataSet.ControlsDisabled then
  begin
  Self.DataSource.DataSet.DisableControls;
  LocalDisabled := true
  end;
  Self.DataSource.DataSet.Bookmark := BM;
  Self.AddBookmark(BM);
  //  RawValue := '[' + RawValue + ']';

  //  Self.FSelectionListObject.RawValueOfIndex := RawValue;
  if  LocalDisabled then Self.DataSource.DataSet.EnableControls;
  Select(RawValue, keepExisting, FireEvent);
  FreeAndNil(CurrentPos);
  end;

  procedure TBMUniDBGrid.Select(BMList: TArray<TBookMark>);
  var
  I: Integer;
  RawValue: String;
  CurrentPos: TBookmark;
  begin
  CurrentPos := Self.DataSource.DataSet.Bookmark;
  Self.DataSource.DataSet.DisableControls;
  Self.ClearSelectionList;

  for I := Low(BMList) to High(BMList) do
  begin
  Self.DataSource.DataSet.Bookmark := BMList[I];
  Self.FSelectionListObject.AddBookmark(BMList[I]);
  RawValue := RawValue + inttostr( Self.DataSource.DataSet.RecNo) + ',';
  end;
  RawValue := RawValue.Remove(RawValue.Length - 1);

  Self.FSelectionListObject.RawValueOfIndex := RawValue;
  Self.Select(RawValue, false, true);
  Self.DataSource.DataSet.EnableControls;
  end;
*)

(*
  procedure TBMUniDBGrid.Select(RawValue: String; const keepExisting: Boolean = False; const FireEvent: Boolean = False);
  var
  MyCode: String;
  begin
  MyCode := Self.JSName + '.getSelectionModel().select([' + RawValue + ']';
  if keepExisting then MyCode := MyCode + ', true' else MyCode := MyCode + ', false';
  if FireEvent then MyCode := MyCode + ', true' else MyCode := MyCode + ', false';
  MyCode := MyCode + ');';
  JSCode(MyCode);
  end;
*)
{$ENDREGION}

procedure TBMUniDBGrid.unselectAll;
begin
  JSCall('getSelectionModel().deselectAll');
end;

procedure TBMUniDBGrid.selectAll;
begin
  JSCode(self.JSName + ' .getSelectionModel().selectAll(false);'
    // +
    // 'var RowsCount = ' +  Self.JSName + '.getSelectionModel().getCount();' +
    // ' ajaxRequest(' + Self.JSName + ', "SelectedRows", ["RowsCount="+  RowsCount]);'
    );
end;

{$REGION 'Removed'}
(*
  procedure TBMUniDBGrid.ClearSelectionList;
  begin
  Self.FSelectionListObject.BookmarkList.Clear;
  end;
*)
{$ENDREGION}

Constructor TBMUniDBGrid.Create(Owner: TComponent);
Begin

{$REGION 'Removed'}
  // Self.FSelectionListObject := TMyBookmarkList.Create;
{$ENDREGION}
  FActionButtons := TActionItems.Create(ActionItem);
  FSearchOptions := TSearchOptions.Create;

  with FSearchOptions do
  begin
    FSearchColumns := '';
    FReconfigureColsMenu := false;
    FSearchButtonTip := '';
    FClearButtonTip := '';
    FReconfigureMenuButtonTip := '';
    FSearchFieldLabel := '';
    FSearchTipText := '';
    FsearchMinLength := 2;
    FSearchFieldWidth := 250;
    FautoSearch := false;
    FAutoSearchTipText := 'Type at least {0} characters';
  end;

  FSearchfeature := false;
  Inherited;
End;

destructor TBMUniDBGrid.Destroy;
begin

{$REGION 'Removed'}
  // FSelectionListObject.Destroy;
{$ENDREGION}

  FActionButtons.Destroy;
  FSearchOptions.Destroy;
  inherited;
end;

{$REGION 'Removed'}
(*
  function TBMUniDBGrid.GetBookmarkList: TArray<TBookMark>;
  begin
  Result := Self.FSelectionListObject.BookmarkList.ToArray;
  end;
*)

(*
  function TBMUniDBGrid.GetRowFielValuedByRowIndex(SelectedRowIndex: Integer;
  FieldName: String): Variant;
  var
  CurrentPos: TBookmark;
  begin
  Self.DataSource.DataSet.DisableControls;
  CurrentPos := Self.DataSource.DataSet.Bookmark;

  DataSource.DataSet.Bookmark := FSelectionListObject.GetBookmark(SelectedRowIndex);
  Result := Self.DataSource.DataSet.FieldByName(FieldName).Value;

  Self.DataSource.DataSet.Bookmark := CurrentPos;
  Self.DataSource.DataSet.EnableControls;
  end;
*)

(*
  function TBMUniDBGrid.GetRowFielValuedByRowIndex(SelectedRowIndex, FieldIndex: Integer): Variant;
  var
  CurrentPos: TBookmark;
  begin
  Self.DataSource.DataSet.DisableControls;
  CurrentPos := Self.DataSource.DataSet.Bookmark;

  DataSource.DataSet.Bookmark := FSelectionListObject.GetBookmark(SelectedRowIndex);
  Result := DataSource.DataSet.Fields[FieldIndex].Value;

  Self.DataSource.DataSet.Bookmark := CurrentPos;
  Self.DataSource.DataSet.EnableControls;
  end;
*)
{$ENDREGION}

procedure TBMUniDBGrid.JSEventHandler(AEventName: String; AParams: TUniStrings);
var
  // CurrentPos: TBookmark;
  Vals: String;
  I: Integer;
begin
  inherited;
  if SameText(AEventName, FActionColumnEvent) then
  begin
    if Assigned(self.OnButtonAction) then
      OnButtonAction(self, AParams.Values['Buttons']);
  end;
{$REGION 'Removed'}
  // else if SameText (AEventName,'SelectedRows') then begin
  // try
  // SelectedRows := StrToInt(AParams.Values['RowsCount']);
  // except
  // SelectedRows := 0 ;
  // end;
  // end
  // else if SameText(AEventName,'selectionchange') then  begin
  // if (DataSource.DataSet.State <> dsinsert)  then  // new add 03/11/2014
  // begin
  // Vals := AParams.Values['ArrayOfIndex'];
  //
  // {$IFDEF COMPILER_17_UP}  //XE3 and highers
  // Vals := Vals.Replace('[', '');
  // Vals := Vals.Replace(']', '');
  // {$ELSE}
  // //Looking for XE2 and lowers compatibility
  // Vals :=   StringReplace( Vals, '[', '', [RfReplaceAll] );
  // Vals :=   StringReplace( Vals, ']', '', [RfReplaceAll] );
  // {$ENDIF}
  //
  // FSelectionListObject.RawArrayOfIndex.CommaText := Vals;
  // DataSource.DataSet.DisableControls;
  // CurrentPos := DataSource.DataSet.Bookmark;
  // ClearSelectionList;
  //
  // for I := 0 to FSelectionListObject.RawArrayOfIndex.Count - 1 do
  // begin
  // DataSource.DataSet.First;
  // DataSource.DataSet.MoveBy(StrToInt( FSelectionListObject.RawArrayOfIndex[I]));
  // FSelectionListObject.AddBookmark(DataSource.DataSet.GetBookmark);
  // end;
  // DataSource.DataSet.GotoBookmark(CurrentPos);
  // DataSource.DataSet.EnableControls;
  //
  // end;
  // end;
{$ENDREGION}
  Vals := EmptyStr;
end;

{$REGION 'MyBookmarkList Removed'}
// procedure TMyBookmarkList.AddBookmark(const BM: TBookmark);
// begin
// if not FBookmarkList.Contains(BM) then
// begin
// FBookmarkList.Add(BM);
// end;
// end;
//
// procedure TMyBookmarkList.ClearBookmarkList;
// begin
// FBookmarkList.Clear;
// FRawArrayOfIndex.Clear;
// end;
//
// function TMyBookmarkList.Count: Integer;
// begin
// Result := FBookmarkList.Count;
// end;
//
// constructor TMyBookmarkList.Create;
// begin
// FBookmarkList := TList<TBookmark>.Create();
// FRawArrayOfIndex := TStringList.Create(true);
// end;
//
// destructor TMyBookmarkList.Destroy;
// begin
// FBookmarkList.Free;
// FRawArrayOfIndex.Free;
// end;
//
//
// function TMyBookmarkList.GetBookmark(const Index: Integer): TBookmark;
// begin
// Result := nil;
// if (Index <= FBookmarkList.Count) then
// Result := Self.FBookmarkList[Index];
// end;

{$ENDREGION}

initialization

UniAddJsLibrary('/files/PagingToolbarResizer.js', true,
  [upoFolderJS, upoPlatformBoth]);
UniAddJsLibrary('/files/ProgressBarPager.js', true,
  [upoFolderJS, upoPlatformBoth]);
UniAddJsLibrary('/files/Searching.js', true, [upoFolderJS, upoPlatformBoth]);

End.
