// ------------------------------------------------------------------------------
// BMUniDBGrid Component
//
// Developed By:  Mike Bresler and  Salvatore Marullo
//
// Email: rullomare@gmail.com
//
// Email: mvbressler2008@gmail.com
//
// Last Update: 2015-02
// Unigui Versione 0.96.,0.97. , 098 Delpi XE3 ,XE4
// ------------------------------------------------------------------------------
//
// Free to use
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//
// Add PopUpMenu  (clone from Mr. PatMan) 2015-02-06
// ------------------------------------------------------------------------------
//
unit BMUniDBGrid.DesignTime;

interface

{$ifdef WIN32}
procedure Register;
{$endif}

implementation
uses
  Classes, BMUniDBGrid;

{$ifdef WIN32}
procedure Register;
begin
  RegisterComponents('uniGUI Custom', [TBMUniDBGrid]);
end;
{$endif}

end.
